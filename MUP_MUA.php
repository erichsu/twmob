<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>MUP Demo build install portal</title>
  <link rel=stylesheet type="text/css" href="styles/style.css">
</head>
<body>

<a name="Top"><div class="congrats">Congrats! You've been invited to the beta of MUP.</div></a>
<br/>

<?php
error_reporting( E_ALL );
ini_set( 'display_errors', 'on' );

date_default_timezone_set('Asia/Taipei');
$plist = 'https://twmob.azurewebsites.net/install_plist.php';

$supportLangs = array('en/' => 'EN', 'ja/' => 'JP', 'tw/' => 'TW');
$lang = isset($_GET['lang']) && array_search($_GET['lang'], $supportLangs)? $_GET['lang']: 'EN';
$dir = array_search($lang, $supportLangs);

$mua_bundle = array('EN' => 'com.trendmicro.mobile.iOS.MUAgentAPN', 'JP' => 'com.trendmicro.mobile.iOS.iTMMS.JPMUAgentAPN', 'TW' => 'com.trendmicro.mobile.iOS.iTMMS.TWMUAgentAPN');
$itmms_bundle = array('EN' => 'com.trendmicro.mobile.iOS.iTMMSAPN', 'JP' => 'com.trendmicro.mobile.iOS.iTMMS.JPAPN', 'TW' => 'com.trendmicro.mobile.iOS.iTMMSAPN');
$jb_bundle = array('EN' => 'com.trendmicro.mobile.iOS.JewleryBox', 'JP' => 'com.trendmicro.mobile.iOS.iTMMS.JPJewleryBox', 'TW' => 'com.trendmicro.mobile.iOS.iTMMS.TWJewleryBox');
$dp_bundle = array('EN' => 'com.trendmicro.mobile.iOS.DirectPass', 'JP' => 'com.trendmicro.mobile.iOS.iTMMS.JPDirectPass', 'TW' => 'com.trendmicro.mobile.iOS.iTMMS.TWDirectPass');

$products = array('MUAgent'    => array('ipa_file' => 'MU_MUA.ipa',
                                        'version_file' => 'MU_MUA',
                                        'ico' => 'images/MUA.png',
                                        'bundle' => $mua_bundle[$lang]));
?>

<h1><?=$lang?></h1>
<div class="step">
  <table>

<?php
foreach($products as $key => $value) {
  $title = $key;
  $file = $value['ipa_file'];
  if (file_exists($dir.$file)) {
    $date = date("Y/m/d H:i:s.", filemtime($dir.$file));
    $version = file_get_contents($dir.$value['version_file']);
    $data = array('url'     => 'https://twmob.azurewebsites.net/'.$dir.$file,
                  'version' => $version,
                  'bundle'  => $value['bundle'],
                  'title'   => $title);
    $url = urlencode($plist.'?'.http_build_query($data));

?>
    <tr>
      <td class="instructions"><?=$title?><br /><?=$version?><br/><?=$date?></td>
      <td width="24" class="arrow">&rarr;</td>
      <td width="57" class="imagelink">
        <a href="itms-services://?action=download-manifest&url=<?=$url?>">
          <img src="<?=$value['ico']?>" height="57" width="57" />
        </a>
      </td>
    </tr>
<?php
  }
}
?>
  </table>
</div>
<br/>

</body>
</html>
