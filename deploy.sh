#!/bin/bash -e
PATH=$PATH:toolkit
## pull
git fetch
git reset --hard origin/master

## Get global builds
sh fetch.sh --prefix "en" --target "MU_iTMMS.ipa" iTMMS
sh fetch.sh --prefix "en" --target "MU_MUA.ipa" MUA
#sh fetch.sh --prefix "en" --target "MU_JB.ipa" JB
#sh fetch.sh --prefix "en" --target "MU_DirectPass.ipa" DP

iTMMS_global="iTMMS Global $(cat en/MU_iTMMS)"
mua_global="MUA Global $(cat en/MU_MUA)"
#jb_global="JB Global $(cat en/MU_JB)"
#dp_global="DP Global $(cat en/MU_DirectPass)"
global="$iTMMS_global\n$mua_global\n$jb_global\n$dp_global"

## Get JP builds
sh fetch.sh --lang ja --prefix "ja" --target "MU_iTMMS.ipa" iTMMS
sh fetch.sh --lang ja --prefix "ja" --target "MU_MUA.ipa" MUA
#sh fetch.sh --filter jp --prefix "ja" --target "MU_JB.ipa" JB
#sh fetch.sh --filter JP --prefix "ja" --target "MU_DirectPass.ipa" DP

iTMMS_jp="iTMMS JP $(cat ja/MU_iTMMS)"
mua_jp="MUA JP $(cat ja/MU_MUA)"
#jb_jp="JB JP $(cat ja/MU_JB)"
#dp_jp="DP JP $(cat ja/MU_DirectPass)"
jp="$iTMMS_jp\n$mua_jp\n$jb_jp\n$dp_jp"

## Get TW builds
sh fetch.sh --lang tw --prefix "tw" --target "MU_iTMMS.ipa" iTMMS
sh fetch.sh --lang tw --prefix "tw" --target "MU_MUA.ipa" MUA
#sh fetch.sh --filter tw --prefix "tw" --target "MU_JB.ipa" JB
#sh fetch.sh --filter TW --prefix "tw" --target "MU_DirectPass.ipa" DP

iTMMS_tw="iTMMS TW $(cat ja/MU_iTMMS)"
mua_tw="MUA TW $(cat ja/MU_MUA)"
#jb_tw="JB TW $(cat ja/MU_JB)"
#dp_tw="DP TW $(cat ja/MU_DirectPass)"
tw="$iTMMS_tw\n$mua_tw\n$jb_tw\n$dp_tw"

## commit changes

message="Update builds. \n\n$global\n\n$jp\n\n$tw"
git commit -a -m"$(echo $message)"

## push
git push
