<?php
/**
 * Examples for how to use CFPropertyList
 * Create the PropertyList sample.xml.plist by using the CFPropertyList API.
 * @package plist
 * @subpackage plist.examples
 */
namespace CFPropertyList;

// just in case...
error_reporting( E_ALL );
ini_set( 'display_errors', 'on' );

/**
 * Require CFPropertyList
 */
require_once(__DIR__.'/CFPropertyList/CFPropertyList.php');


/*
 * create a new CFPropertyList instance without loading any content
 */
$plist = new CFPropertyList();

/*
 * Manuall Create the sample.xml.plist
 */
// the Root element of the PList is a Dictionary
$plist->add( $dict = new CFDictionary() );

$dict->add( 'items', $array = new CFArray() );
$array->add( $dict2 = new CFDictionary() );
$dict2->add( 'assets', $assetsArray = new CFArray() );

$assetsArray->add( $assetsDict = new CFDictionary() );
$assetsDict->add( 'kind', new CFString( 'software-package' ) );
$assetsDict->add( 'url', new CFString( $_GET['url'] ) );

$dict2->add( 'metadata', $metadataDict = new CFDictionary() );
// $metadataDict->add( 'bundle-identifier', new CFString( 'com.trendmicro.mobile.iTMMSAPN' ) );
$metadataDict->add( 'bundle-identifier', new CFString( $_GET['bundle'] ) );
$metadataDict->add( 'bundle-version', new CFString( $_GET['version'] ) );
$metadataDict->add( 'kind', new CFString( 'software' ) );
$metadataDict->add( 'title', new CFString( $_GET['title'] ) );

print $plist->toXML();
?>
