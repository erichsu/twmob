<?php
/*
 * Setup default time zone
 */
date_default_timezone_set('Asia/Taipei');

$title = 'iTMMS2';
$host = 'https://twmob.azurewebsites.net/';
$plist = 'https://twmob.azurewebsites.net/install_plist.php';
$builds = array(array('dir' => 'iTMMS2/eicar/', 'bundle' => 'com.trendmicro.mobile.iTMMSAPN', 'type' => 'EICAR'),
                array('dir' => 'iTMMS2/old_beta/', 'bundle' => 'com.trendmicro.mobile.iTMMSAPN', 'type' => 'Old Beta'),
                array('dir' => 'iTMMS2/en_beta/', 'bundle' => 'com.trendmicro.mobile.iOS.iTMMSAPN', 'type' => 'Global Beta'),
                array('dir' => 'iTMMS2/ja_beta/', 'bundle' => 'com.trendmicro.mobile.iOS.iTMMS.JPAPN', 'type' => 'JA Beta'),
                array('dir' => 'iTMMS2/apac_beta/', 'bundle' => 'com.trendmicro.mobile.iOS.APAC.iTMMS', 'type' => 'APAC Beta'),
                array('dir' => 'iTMMS2/emea_beta/', 'bundle' => 'com.trendmicro.mobile.iOS.EMEA.iTMMS', 'type' => 'EMEA Beta'));
?>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <title>Install iTMMS 2.0</title>
  <link rel=stylesheet type="text/css" href="styles/style.css">
</head>
<body>
<?php
foreach($builds as $build) {
?>
<a href="#<?=$build['type']?>"><?=$build['type']?></a>
<?php } ?>

<br/>

<?php
foreach($builds as $build) {
?>

<a name="<?=$build['type']?>"><h2><?=$build['type']?></h2></a><a href="#Top">Top</a>
<?php
  $files = array();
  if ($handle = opendir($build['dir'])) {
    while (false !== ($file = readdir($handle))) {
      if ($file != "." && $file != "..") {
        $files[]=$file;
      }
    }
    closedir($handle);
    arsort($files);
  }

  foreach($files as $file) {
    $date = date("Y/m/d H:i:s.", filemtime($build['dir'] . $file));
    $version = explode("_", $file)[3];
    $data = array('url'   => $host.$build['dir'].$file,
                  'title' => $title,
                  'version' => $version,
                  'bundle' => $build['bundle']);
    $url = urlencode($plist.'?'.http_build_query($data));
?>
<div class="step">
  <table><tr>
    <td class="instructions">Install the<br />iTMMS 2.0 app<br />Build: <?=$version?><br/>Release Date: <?=$date?></td>
    <td width="24" class="arrow">&rarr;</td>
    <td width="57" class="imagelink">
      <a href="itms-services://?action=download-manifest&url=<?=$url?>">
        <img src="images/img_about_tmms.png" height="57" width="57" />
      </a>
    </td>
  </tr></table>
</div>
<?php
  }
}
?>
</body>
</html>
